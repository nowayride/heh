# README #

"heh" (because I lack originality) came about from me being tired of writing convoluted bash commands every time I needed to do something that should be trivial, but not always being able to rely on Ansible without bloating up Docker images. I set to make a concise and portable script for some of the more popular deployment actions, such as pulling and extracting an archive off the internet, though Docker's ADD does this as well. The driving factor for this was having to install a package, run a command, then remove the package cache every time I build a Dockerfile. As an alternative, ADD heh then sh heh install-clean something and let the script do all of the doity woik for you.

Pull the file down, or don't because I couldn't care less. Chmod it, run with /bin/ash, whatever. You can even just source the file and run the commands directly (except install because it's an intuitive command name for essentially a copy command, thx BSD). Help has most of what you need, and since I've already written it once I'll just copy and paste:

```
Available commands:
 -- facts --------- output facts in openrc format, set with 'source <(heh facts)'
 -- install ------- install a package
 -- install-clean - install a package and clean the package cache
 -- remove -------- remove a package
 -- clean --------- clean the system cache
 -- unarchive ----- extract an archive from disk or URL
 -- fetch --------- grabs a file from URL, defaults to stdout
 -- help ---------- you're porkin at it
```

A nice trick: source <(heh facts). Now you have all of the variables in env. \o/

I plan to keep adding modules but it will likely never get as featured as other deployment systems will. "heh" should remain mainly systems oriented, such as firewall rules, package management, permissions, file handling, etc and likely won't get much into application level details. It's also highly unlikely I will imlement any sort of templating system since they are not easily portable. Install jinja if that's needed. I might replicate the lineinfile functionality from Ansible.